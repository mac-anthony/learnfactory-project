import React, {Component} from 'react'


export default class Template2 extends Component {
    state = {count: 0};
    addOne = () => {
        this.setState((prevState) => ({
            count:prevState.count + 1
        }))
    };

    minusOne = () => {
        this.setState((prevState) => ({
            count:prevState.count - 1
        }))
    };

    Reset = () => {
        this.setState((prevState) => ({
            count:this.setState.count = 0
        }))
    }
    
    render(){
        return(
            <div>
                <h1>Count: {this.state.count}</h1>

                <button onClick={this.addOne}>+1</button>

                <button onClick={this.minusOne}>-1</button>
               
                <button onClick={this.Reset}>Reset</button>

            </div>
        )
    }
}