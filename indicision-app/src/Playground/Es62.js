import React, {Component} from 'react';


export default class Es62 extends Component{
    render(){
        const multiplier = {
            numbers: [10,20,30],
            multiplyBy: 2,
            multiply(){
                return this.numbers.map((number) => number * this.multiplyBy);
            }
        };
        {console.log(multiplier.multiply())}

        const user = {
            name: 'BOBOITO',
            cities: ['Awka','Port-Harcourt','Aba','Onitsha','Asaba'],
            printPlacesLived() {
                // this.cities.forEach((city) => {
                //     console.log(this.name + ' has lived in ' + city);

                // }); 
                //we can also show places leaved using the map method below
                return this.cities.map((city) => this.name + ' has lived in ' + city);
            }
        };
       // user.printPlacesLived();this is for the forEach
       console.log(user.printPlacesLived());//this is for the map

        return(
            <div>
            
            
            </div>
        )
    }
}