import React, { Component } from 'react';
import logo from '../logo.svg';
import '../styles/App.css';
import Es62 from '../Playground/Es62';
import Template2 from '../Playground/Template2'

class App extends Component {
  render() {
  const userName = 'Welcome to my Indecision App';
  let userAge = 20;
  let userLocation = 'Russia';
  let userSex = 'Male' 
  //or you can also use this method below

  const user = {
    name: 'Dickson Mac',
    age: ''
  };

  function getName(name) {
    if (name) {
      return name;
    }else{
      return 'Unknown';
    }
  }

  

    
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{userName.toUpperCase()} </h1>
        </header>
        <p className="App-intro">
          This is some info for you down below 
          
        </p>

        <ol>
          <li> <strong> Name:</strong> {getName(user.name)} </li>          
          <li> <strong> Age:</strong> {user.age ? user.age : 'Anonymous'} </li>
          <li> <strong>Location:</strong> {userLocation} </li>
          <li> <strong>Sex:</strong> {userSex} </li>
         
        </ol>
        
        <form >
          <input type="text" name="option" />
          <button>Add Option</button>
        </form>



        <Es62 />
        <Template2 />
      </div>
    );
  }
}

export default App;
